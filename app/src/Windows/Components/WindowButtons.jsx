import styled from 'styled-components';

const WindowButtons = styled.div`
  ${props => props.isWindowsOrLinux ? `
    width: auto;
    height: 32px;
    display: ${props.isCustomTitlebar ? 'flex' : 'none'};
    position: absolute;
    top: 1px;
    right: 0px;
    box-sizing: border-box;
  ` : `
    width: 68px;
    height: 32px;
    display: ${props.isCustomTitlebar ? 'flex' : 'none'};
    position: absolute;
    top: 1px;
    padding: 0px 10px;
    border-right: solid 0.5px #8b8b8b;
    justify-content: space-between;
    align-items: center;
    box-sizing: border-box;
  `}
`;

export default WindowButtons;