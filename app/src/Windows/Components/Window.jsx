import styled from 'styled-components';

const Window = styled.div`
  width: 100vw;
  height: 100vh;
  border: ${props => props.isMaximized ? 'none' : props.isCustomTitlebar ? 'solid 1px #8b8b8b' : 'none'};
`;

export default Window;