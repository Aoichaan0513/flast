import styled from 'styled-components';

const TabContent = styled.div`
  display: ${props => props.isActive ? 'block' : 'none'};
  width: 100%;
  height: 100%;
  box-sizing: border-box;
`;

export default TabContent;