import React, { Component } from 'react';

import Window from './Components/Window';
import WindowButtons from './Components/WindowButtons';
import WindowButton from './Components/WindowButton';
import WindowContent from './Components/WindowContent';
import Titlebar from './Components/Titlebar';

import Button from './Components/Button';

const { remote, ipcRenderer, shell } = window.require('electron');
const systemPreferences = remote.systemPreferences;

const platform = require('electron-platform');

const Config = window.require('electron-store');
const config = new Config();

class AuthenticationWindow extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userName: '',
			passWord: ''
		};

		ipcRenderer.on('window-change-settings', (e, args) => {
			this.forceUpdate();
		});

		this.setState({ passWord: '' });
		document.title = '認証';
	}

	submit = () => {
		ipcRenderer.send(`authWindow-result-${this.props.match.params.windowId}`, { user: this.state.userName, pass: this.state.passWord });
	}

	getForegroundColor = (hexColor) => {
		var r = parseInt(hexColor.substr(1, 2), 16);
		var g = parseInt(hexColor.substr(3, 2), 16);
		var b = parseInt(hexColor.substr(5, 2), 16);

		return ((((r * 299) + (g * 587) + (b * 114)) / 1000) < 128) ? '#ffffff' : '#000000';
	}

	render() {
		return (
			<div style={{ boxSizing: 'border-box', margin: '0px 4px', padding: 8, boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.16), 0px 2px 4px rgba(0, 0, 0, 0.23)', borderRadius: 3, backgroundColor: 'white', width: 'auto', display: 'flex', flexDirection: 'column', justifyContent: 'space-around' }}>
				<div style={{ marginBottom: 10 }}>
					<label style={{ whiteSpace: 'normal' }}>このサイトにログインするための認証情報を入力してください。</label>
					<label style={{ marginBottom: 0 }}>ユーザー名</label>
					<input type="text" value={this.state.userName} onChange={(e) => { this.setState({ userName: e.target.value }); }} style={{ width: '100%', cursor: 'initial', paddingLeft: 3, paddingRight: 3 }} />
					<label style={{ marginTop: 5, marginBottom: 0 }}>パスワード</label>
					<input type="password" value={this.state.passWord} onChange={(e) => { this.setState({ passWord: e.target.value }); }} style={{ width: '100%', cursor: 'initial', paddingLeft: 3, paddingRight: 3 }} />
				</div>
				<div style={{ display: 'flex', justifyContent: 'space-between' }}>
					<Button style={{ width: '49.2%' }} onClick={this.submit.bind(this)}>送信</Button>
					<Button style={{ width: '49.2%' }} onClick={() => ipcRenderer.send(`authWindow-close-${this.props.match.params.windowId}`, {})}>キャンセル</Button>
				</div>
			</div>
		);
	}
}

export default AuthenticationWindow;