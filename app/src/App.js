import React, { Component } from 'react';
import { HashRouter, Route, Switch } from "react-router-dom";

import MainWindow from './Windows/MainWindow';
import InfomationWindow from './Windows/InfomationWindow';
import PermissionWindow from './Windows/PermissionWindow';
import MenuWindow from './Windows/MenuWindow';

import SuggestWindow from './Windows/SuggestWindow';
import AuthenticationWindow from './Windows/AuthenticationWindow';

import ApplicationWindow from './Windows/ApplicationWindow';

class App extends Component {
	render() {
		return (
			<HashRouter>
				<Route exact path='/window/:windowId/:url?' component={MainWindow} />
				<Route path='/info/:windowId' component={InfomationWindow} />
				<Route path='/permission/:windowId' component={PermissionWindow} />
				<Route path='/menu/:windowId/:tabId' component={MenuWindow} />

				<Route path='/suggest/:windowId' component={SuggestWindow} />
				<Route path='/authentication/:windowId' component={AuthenticationWindow} />

				<Route path='/app/:windowId/:url' component={ApplicationWindow} />
			</HashRouter>
		);
	}
}

export default App;