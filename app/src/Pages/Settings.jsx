import React, { Component } from 'react';
import ReactDOM, { render } from 'react-dom';
import { SnackbarProvider, withSnackbar } from 'notistack';
import { ChromePicker } from 'react-color';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Snackbar from '@material-ui/core/Snackbar';
import Popover from '@material-ui/core/Popover';

import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';

import CloseIcon from '@material-ui/icons/CloseOutlined';
import ReloadIcon from '@material-ui/icons/RefreshOutlined';
import ExpandLessIcon from '@material-ui/icons/ExpandLessOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMoreOutlined';

import MovieIcon from '@material-ui/icons/MovieOutlined';
import GeolocationIcon from '@material-ui/icons/LocationOnOutlined';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import RadioIcon from '@material-ui/icons/RadioOutlined';
import LockIcon from '@material-ui/icons/LockOutlined';
import FullscreenIcon from '@material-ui/icons/FullscreenOutlined';
import LaunchIcon from '@material-ui/icons/LaunchOutlined';

import NavigationBar from './Components/NavigationBar.jsx';

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15)
    },
    button: {
        margin: theme.spacing(1),
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    snackBarClose: {
        padding: theme.spacing(0.5),
    },
});

class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isDialogOpened: false,
            isExpanded: null,
            colorPickerAnchorEl: null,

            // Design
            isHomeButton: false,
            isBookmarkBar: false,
            isDarkTheme: false,
            tabAccentColor: '#0a84ff',
            isCustomTitlebar: false,
            isCustomTitlebar2: false,
            theme: 'default',

            // HomePage
            isDefaultHomePage: false,
            homePage: 'None',

            // Search Engine
            searchEngine: 'None',
            searchEngines: [],

            // Page Settings
            isMedia: -1,
            isGeolocation: -1,
            isNotifications: -1,
            isMidiSysex: -1,
            isPointerLock: -1,
            isFullScreen: -1,
            isOpenExternal: -1,

            // AdBlock
            isAdBlock: false,

            // About
            updateStatus: 'checking',

            // TextInput
            homePageValue: '',

            // Snackbar
            isShowingSnackbar: false,
            snackBarDuration: 1000 * 3,
            snackBarText: ''
        };
    }

    componentDidMount = () => {
        this.setState({
            // Design
            isHomeButton: window.getHomeButton(),
            isBookmarkBar: window.getBookmarkBar(),
            isDarkTheme: window.getDarkTheme(),
            tabAccentColor: window.getTabAccentColor(),
            isCustomTitlebar: window.getCustomTitlebar(),
            isCustomTitlebar2: window.getCustomTitlebar(),

            // HomePage
            isDefaultHomePage: window.getDefaultHomePage(),
            homePage: window.getStartPage(false),

            // Search Engine
            searchEngine: window.getSearchEngine().name,

            // Page Settings
            isMedia: window.getMedia(),
            isGeolocation: window.getGeolocation(),
            isNotifications: window.getNotifications(),
            isMidiSysex: window.getMidiSysex(),
            isPointerLock: window.getPointerLock(),
            isFullScreen: window.getFullScreen(),
            isOpenExternal: window.getOpenExternal(),

            // AdBlock
            isAdBlock: window.getAdBlock(),

            // Other
            homePageValue: window.getStartPage(false),
        });

        window.getSearchEngines().forEach((item, i) => {
            this.setState(state => { return { searchEngines: [...state.searchEngines, item] }; });
        });

        window.getUpdateStatus().then((result) => {
            this.setState({ updateStatus: result });
        });
        setInterval(() => {
            this.setState({ updateStatus: 'checking' });
            window.getUpdateStatus().then((result) => {
                this.setState({ updateStatus: result });
            });
        }, 1000 * 30);

        if (window.location.hash !== '') {
            this.setState({ isExpanded: window.location.hash.substring(1) });
        }
    }

    componentWillReceiveProps = () => {
        if (window.location.hash !== '') {
            this.setState({ isExpanded: window.location.hash.substring(1) });
        }
    }

    handleChange = (panel) => (e, isExpanded) => {
        this.setState({ isExpanded: isExpanded ? panel : false });
    }

    handleDialogClose = () => {
        this.setState({ isDialogOpened: false });
    }

    handleSnackbarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ isShowingSnackbar: false });
    }

    handleColorPicker = (e) => {
        this.setState({ colorPickerAnchorEl: e.currentTarget });
    }

    handleColorPickerClose = () => {
        this.setState({ colorPickerAnchorEl: null });
    }

    handleColorPickerChangeComplete = (color) => {
        this.setState({ tabAccentColor: color.hex });
        window.setTabAccentColor(color.hex);

        this.props.enqueueSnackbar('設定を変更しました。', {
            variant: 'success',
        });
    };

    render() {
        const { classes } = this.props;

        return (
            <NavigationBar title="設定" buttons={[<Button color="inherit" onClick={() => { window.openInEditor(); }}>テキストエディタで開く</Button>]}>
                <Container fixed>
                    <ExpansionPanel expanded={this.state.isExpanded === 'design'} onChange={this.handleChange('design')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="design-content"
                            id="design-header"
                        >
                            <Typography className={classes.heading}>デザイン</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={10} sm={11} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        ホームボタンを表示する
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} sm={1} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <Switch
                                        checked={this.state.isHomeButton}
                                        onChange={(e) => {
                                            this.setState({ ...this.state, isHomeButton: e.target.checked });
                                            window.setHomeButton(e.target.checked);

                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                        color="primary"
                                        value="isHomeButton"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={10} sm={11} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        ブックマーク バーを表示する
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} sm={1} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <Switch
                                        checked={this.state.isBookmarkBar}
                                        onChange={(e) => {
                                            this.setState({ ...this.state, isBookmarkBar: e.target.checked });
                                            window.setBookmarkBar(e.target.checked);

                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                        color="primary"
                                        value="isBookmarkBar"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={10} sm={11} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        ダーク テーマを使用する
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} sm={1} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <Switch
                                        checked={this.state.isDarkTheme}
                                        onChange={(e) => {
                                            this.setState({ ...this.state, isDarkTheme: e.target.checked });
                                            window.setDarkTheme(e.target.checked);

                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                        color="primary"
                                        value="isDarkTheme"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={7} sm={9} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        タブのアクセントカラー
                                    </Typography>
                                </Grid>
                                <Grid item xs={5} sm={3} style={{ display: 'flex', padding: '0px 8px' }} alignItems="center" justify="flex-end">
                                    <Button variant="link" style={{ marginRight: 5 }}
                                        onClick={() => {
                                            this.setState({ tabAccentColor: '#0a84ff' }); window.setTabAccentColor('#0a84ff');
                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                    >
                                        リセット
                                    </Button>
                                    <Button variant="outlined" onClick={this.handleColorPicker}>色を選択</Button>
                                    <Popover
                                        id={Boolean(this.state.colorPickerAnchorEl) ? 'colorPickerPopOver' : undefined}
                                        open={Boolean(this.state.colorPickerAnchorEl)}
                                        anchorEl={this.state.colorPickerAnchorEl}
                                        onClose={this.handleColorPickerClose}
                                        anchorOrigin={{
                                            vertical: 'bottom',
                                            horizontal: 'right',
                                        }}
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                    >
                                        <ChromePicker
                                            color={this.state.tabAccentColor}
                                            onChangeComplete={this.handleColorPickerChangeComplete} />
                                    </Popover>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={7} sm={9} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        カスタムタイトルバーを使用する
                                    </Typography>
                                </Grid>
                                <Grid item xs={5} sm={3} style={{ display: 'flex', padding: '0px 8px' }} alignItems="center" justify="flex-end">
                                    {this.state.isCustomTitlebar2 !== window.getCustomTitlebar() ?
                                        <Button variant="outlined" className={classes.button} onClick={() => { window.restart(); }}>再起動</Button>
                                        :
                                        null
                                    }
                                    <Switch
                                        checked={this.state.isCustomTitlebar}
                                        onChange={(e) => {
                                            this.setState({ ...this.state, isCustomTitlebar: e.target.checked });
                                            window.setCustomTitlebar(e.target.checked);

                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                        color="primary"
                                        value="isCustomTitlebar"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={8} sm={8} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        ウィンドウの詳細設定
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <Button variant="outlined">変更</Button>
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'homePage'} onChange={this.handleChange('homePage')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="homePage-content"
                            id="homePage-header"
                        >
                            <Typography className={classes.heading}>ホームページ</Typography>
                            <Typography color="textSecondary" className={classes.secondaryHeading}>現在、{this.state.isDefaultHomePage ? 'デフォルトホームページ' : 'カスタムページ'} を使用しています</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={12} style={{ padding: '8px 14px' }}>
                                    <div className={classes.formRoot}>
                                        <FormControl component="fieldset" className={classes.formControl}>
                                            <RadioGroup
                                                aria-label="Gender"
                                                onChange={(e) => {
                                                    this.setState({ ...this.state, isDefaultHomePage: (e.target.value === 'default') });
                                                    window.setDefaultHomePage(e.target.value === 'default');

                                                    if (e.target.value === 'default') {
                                                        this.props.enqueueSnackbar('設定を変更しました。', {
                                                            variant: 'success',
                                                        });
                                                    } else {
                                                        this.props.enqueueSnackbar('リンクを入力し、Enterキーを押すまで設定は反映されません。', {
                                                            variant: 'warning',
                                                        });
                                                    }
                                                }}
                                                value={this.state.isDefaultHomePage ? 'default' : 'custom'}
                                            >
                                                <FormControlLabel value="default" control={<Radio color="primary" />} label={<span><Link href="flast://home/">ホーム</Link> ページ</span>} />
                                                <FormControlLabel value="custom" control={<Radio color="primary" />} label="カスタムページ" />
                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                    {!this.state.isDefaultHomePage ?
                                        <TextField
                                            id="standard-full-width"
                                            style={{ width: '-webkit-fill-available', margin: 8 }}
                                            label="カスタムページ リンクを入力"
                                            placeholder="カスタムページ リンクを入力"
                                            margin="normal"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            value={this.state.homePageValue}
                                            onChange={(e) => { this.setState({ ...this.state, homePageValue: e.target.value }); }}
                                            onKeyDown={(e) => {
                                                if (e.key != 'Enter') return;

                                                if (window.isURL(this.state.homePageValue) && !this.state.homePageValue.includes('://')) {
                                                    window.setStartPage(`http://${this.state.homePageValue}`);
                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                } else if (!this.state.homePageValue.includes('://')) {
                                                    window.setDefaultHomePage(true);
                                                    window.setStartPage('flast://home');
                                                    this.props.enqueueSnackbar('値が空かURLではないためデフォルトホーム ページが設定されました。', {
                                                        variant: 'warning',
                                                    });
                                                } else {
                                                    window.setStartPage(this.state.homePageValue);
                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }
                                            }}
                                        />
                                        :
                                        null
                                    }
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'search'} onChange={this.handleChange('search')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="search-content"
                            id="search-header"
                        >
                            <Typography className={classes.heading}>検索エンジン</Typography>
                            <Typography color="textSecondary" className={classes.secondaryHeading}>現在、{window.getSearchEngine().name} を使用しています</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={8} sm={8} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        アドレスバーと<Link href="flast://home/">ホーム</Link> ページで使用される検索エンジン
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.searchEngine}
                                                onChange={(e) => {
                                                    this.setState({ searchEngine: e.target.value });
                                                    window.setSearchEngine(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'searchEngine',
                                                    id: 'searchEngine',
                                                }}
                                            >
                                                {this.state.searchEngines.map((item, i) => {
                                                    return (
                                                        <MenuItem key={i} value={item.name}>{item.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                        <Divider />
                        <ExpansionPanelActions>
                            <Button variant="outlined" size="small">検索エンジンの管理</Button>
                        </ExpansionPanelActions>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'pageSettings'} onChange={this.handleChange('pageSettings')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="pageSettings-content"
                            id="pageSettings-header"
                        >
                            <Typography className={classes.heading}>ページ設定</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <MovieIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        メディア
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isMedia}
                                                onChange={(e) => {
                                                    this.setState({ isMedia: e.target.value });
                                                    window.setMedia(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'media',
                                                    id: 'media',
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <GeolocationIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        位置情報の使用
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isGeolocation}
                                                onChange={(e) => {
                                                    this.setState({ isGeolocation: e.target.value });
                                                    window.setGeolocation(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'geolccation',
                                                    id: 'geolocation',
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <NotificationsIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        通知の送信
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isNotifications}
                                                onChange={(e) => {
                                                    this.setState({ isNotifications: e.target.value });
                                                    window.setNotifications(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'notifications',
                                                    id: 'notifications',
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <RadioIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        MIDI デバイスへのアクセス
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isMidiSysex}
                                                onChange={(e) => {
                                                    this.setState({ isMidiSysex: e.target.value });
                                                    window.setMidiSysex(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'midiSysex',
                                                    id: 'midiSysex',
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <LockIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        マウス カーソルの固定
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isPointerLock}
                                                onChange={(e) => {
                                                    this.setState({ isPointerLock: e.target.value });
                                                    window.setPointerLock(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'pointerLock',
                                                    id: 'pointerLock',
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <FullscreenIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        全画面表示
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isFullScreen}
                                                onChange={(e) => {
                                                    this.setState({ isFullScreen: e.target.value });
                                                    window.setFullScreen(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'fullScreen',
                                                    id: 'fullScreen'
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={1} sm={1} style={{ padding: '8px 14px' }}>
                                    <LaunchIcon />
                                </Grid>
                                <Grid item xs={7} sm={7} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2" style={{ lineHeight: 1.8 }}>
                                        外部リンクを開く
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.isOpenExternal}
                                                onChange={(e) => {
                                                    this.setState({ isOpenExternal: e.target.value });
                                                    window.setOpenExternal(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'openExternal',
                                                    id: 'openExternal'
                                                }}
                                            >
                                                <MenuItem value={-1}>デフォルト (毎回確認する)</MenuItem>
                                                <MenuItem value={0}>ブロック</MenuItem>
                                                <MenuItem value={1}>許可</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'adblock'} onChange={this.handleChange('adblock')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="adblock-content"
                            id="adblock-header"
                        >
                            <Typography className={classes.heading}>広告ブロック</Typography>
                            <Typography color="textSecondary" className={classes.secondaryHeading}>広告ブロックは{window.getAdBlock() ? '有効' : '無効'}です</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={10} sm={11} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        広告ブロックを使用する
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} sm={1} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <Switch
                                        checked={this.state.isAdBlock}
                                        onChange={(e) => {
                                            this.setState({ ...this.state, isAdBlock: e.target.checked });
                                            window.setAdBlock(e.target.checked);

                                            this.props.enqueueSnackbar('設定を変更しました。', {
                                                variant: 'success',
                                            });
                                        }}
                                        color="primary"
                                        value="isAdBlock"
                                    />
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                        <Divider />
                        <ExpansionPanelActions>
                            <Button variant="outlined" size="small">ページ設定</Button>
                            <Button variant="outlined" size="small"
                                onClick={() => {
                                    window.updateFilters();

                                    this.props.enqueueSnackbar('定義ファイルのアップデートと再読み込みをしています…', {
                                        variant: 'info',
                                    });
                                }}>
                                定義ファイルのアップデート・再読み込み
                            </Button>
                        </ExpansionPanelActions>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'language'} onChange={this.handleChange('language')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="language-content"
                            id="language-header"
                        >
                            <Typography className={classes.heading}>言語</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={8} sm={8} style={{ padding: '8px 14px' }}>
                                    <Typography variant="body2">
                                        アドレスバーと<Link href="flast://credits/">新しいタブ</Link> ページで使用される検索エンジン
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} sm={4} style={{ display: 'flex', padding: '0px 8px' }} direction="column" alignItems="flex-end">
                                    <div className={classes.formRoot}>
                                        <FormControl className={classes.formControl}>
                                            <Select
                                                value={this.state.searchEngine}
                                                onChange={(e) => {
                                                    this.setState({ searchEngine: e.target.value });
                                                    window.setSearchEngine(e.target.value);

                                                    this.props.enqueueSnackbar('設定を変更しました。', {
                                                        variant: 'success',
                                                    });
                                                }}
                                                inputProps={{
                                                    name: 'searchEngine',
                                                    id: 'searchEngine',
                                                }}
                                            >
                                                {this.state.searchEngines.map((item, i) => {
                                                    return (
                                                        <MenuItem key={i} value={item.name}>{item.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                        <Divider />
                        <ExpansionPanelActions>
                            <Button variant="outlined" size="small">検索エンジンの管理</Button>
                        </ExpansionPanelActions>
                    </ExpansionPanel>
                    <ExpansionPanel expanded={this.state.isExpanded === 'about'} onChange={this.handleChange('about')}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="about-content"
                            id="about-header"
                        >
                            <Typography className={classes.heading}>{window.getAppName()} について</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={2}>
                                <Grid item style={{ width: 96, padding: '0px 16px' }}>
                                    <img src="flast-file:///icon.png" width="78" height="78" />
                                </Grid>
                                <Grid item style={{ padding: '0px 16px' }} alignItems="center">
                                    <Typography variant="subtitle1" gutterBottom>{window.getAppName()}</Typography>
                                    {(() => {
                                        if (this.state.updateStatus === 'checking') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>アップデートを確認しています…</Typography>
                                            );
                                        } else if (this.state.updateStatus === 'available') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>アップデートが見つかりました</Typography>
                                            );
                                        } else if (this.state.updateStatus === 'not-available') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>{window.getAppName()} は最新版です</Typography>
                                            );
                                        } else if (this.state.updateStatus === 'error') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>アップデートをが確認できませんでした</Typography>
                                            );
                                        } else if (this.state.updateStatus === 'downloading') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>アップデートをダウンロードしています…</Typography>
                                            );
                                        } else if (this.state.updateStatus === 'downloaded') {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>再起動して {window.getAppName()} のアップデートを適用してください</Typography>
                                            );
                                        } else {
                                            return (
                                                <Typography variant="subtitle2" gutterBottom>アップデートを確認しています…</Typography>
                                            );
                                        }
                                    })()}
                                    <Typography variant="body2" color="textSecondary" gutterBottom>
                                        バージョン: {window.getAppVersion()} ({window.getAppChannel()}) (Electron: {window.getElectronVersion()}, Chromium: {window.getChromiumVersion()})
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={12} style={{ padding: '0px 16px' }}>
                                    <Typography variant="body2" color="textSecondary" gutterBottom style={{ marginBottom: '0.8em' }}>
                                        {window.getAppName()}<br />
                                        {window.getAppDescription()}<br />
                                        Copyright 2019 Aoichaan0513. All rights reserved.
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" gutterBottom>
                                        {window.getAppName()} はChromiumやその他の<Link href="flast://credits/">オープンソース ソフトウェア</Link>によって実現しました。
                                    </Typography>
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                        <Divider />
                        <ExpansionPanelActions>
                            <Button variant="outlined" size="small" onClick={() => { this.setState({ isDialogOpened: true }); }}>リセット</Button>
                        </ExpansionPanelActions>
                    </ExpansionPanel>
                </Container>
                <Dialog
                    open={this.state.isDialogOpened}
                    onClose={this.handleDialogClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">データのリセット</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            本当にデータをリセットしてよろしいですか？<br />
                            <b>続行</b>を押すとデータのリセット後アプリが再起動します。
                        </DialogContentText>
                        <Divider />
                        <Typography variant="subtitle2" gutterBottom>削除されるデータ</Typography>
                        <ul>
                            <li>履歴</li>
                            <li>ブックマーク (プライベート ブックマークも含む)</li>
                            <li>キャッシュ</li>
                            <li>Cookieとサイトデータ</li>
                            <ul>
                                <li>ログイン情報</li>
                            </ul>
                        </ul>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" variant="outlined" onClick={this.handleDialogClose} style={{ marginRight: 5 }}>
                            キャンセル
                        </Button>
                        <Button color="primary" variant="contained" onClick={() => { window.clearBrowserData(true); window.restart(); }}>
                            続行
                        </Button>
                    </DialogActions>
                </Dialog>
                <Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                    open={this.state.isShowingSnackbar}
                    autoHideDuration={this.state.snackBarDuration}
                    onClose={this.handleSnackbarClose}
                    ContentProps={{ 'aria-describedby': 'message-id' }}
                    message={<span id="message-id">{this.state.snackBarText}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.handleSnackbarClose}
                        >
                            <CloseIcon />
                        </IconButton>
                    ]}
                />
            </NavigationBar>
        );
    }
}

Settings.propTypes = {
    classes: PropTypes.object.isRequired,
};

const Page = withSnackbar(withStyles(styles)(Settings));

render(
    <SnackbarProvider maxSnack={3} autoHideDuration={3000}>
        <Page />
    </SnackbarProvider>,
    document.getElementById('app')
);