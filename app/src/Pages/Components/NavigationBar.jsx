import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { createMuiTheme, withStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import PublicIcon from '@material-ui/icons/PublicOutlined';

import HomeIcon from '@material-ui/icons/HomeOutlined';
import HistoryIcon from '@material-ui/icons/HistoryOutlined';
import DownloadsIcon from '@material-ui/icons/GetAppOutlined';
import BookmarksIcon from '@material-ui/icons/BookmarksOutlined';
import AppsIcon from '@material-ui/icons/AppsOutlined';
import ShopIcon from '@material-ui/icons/ShopOutlined';
import SettingsIcon from '@material-ui/icons/SettingsOutlined';
import HelpIcon from '@material-ui/icons/HelpOutlineOutlined';
import InfoIcon from '@material-ui/icons/InfoOutlined';

import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';

const lightTheme = createMuiTheme({
    palette: {
        type: 'light'
    },
});
const darkTheme = createMuiTheme({
    palette: {
        type: 'dark'
    },
});

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    title: {
        flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    link: {
        color: 'inherit',
        textDecoration: 'none'
    },
    content: {
        width: `calc(100% - ${drawerWidth}px)`,
        flexGrow: 1,
        padding: theme.spacing(3),
    },
});

class NavigationBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            mobileOpen: false
        };
    }

    componentDidMount = () => {
        if ('settings' === window.location.host) {
            this.setState(state => ({ open: true }));
        }
    }

    handleNestedListClick = () => {
        this.setState(state => ({ open: !state.open }));
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    }

    render() {
        const { classes, theme } = this.props;

        const drawer = (
            <ThemeProvider theme={window.getDarkTheme() ? darkTheme : lightTheme}>
                <List>
                    <ListItem>
                        <ListItemIcon><PublicIcon /></ListItemIcon>
                        <ListItemText primary="Flast" />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <a className={classes.link} href={window.getStartPage()} >
                        <ListItem button selected={'flast://home/' === window.location.href}>
                            <ListItemIcon><HomeIcon /></ListItemIcon>
                            <ListItemText primary="ホーム" />
                        </ListItem>
                    </a>
                </List>
                <Divider />
                <List>
                    <a className={classes.link} href="flast://history/">
                        <ListItem button selected={'flast://history/' === window.location.href}>
                            <ListItemIcon><HistoryIcon /></ListItemIcon>
                            <ListItemText primary="履歴" />
                        </ListItem>
                    </a>
                    <a className={classes.link} href="flast://downloads/">
                        <ListItem button selected={'flast://downloads/' === window.location.href}>
                            <ListItemIcon><DownloadsIcon /></ListItemIcon>
                            <ListItemText primary="ダウンロード" />
                        </ListItem>
                    </a>
                    <a className={classes.link} href="flast://bookmarks/">
                        <ListItem button selected={'flast://bookmarks/' === window.location.href}>
                            <ListItemIcon><BookmarksIcon /></ListItemIcon>
                            <ListItemText primary="ブックマーク" />
                        </ListItem>
                    </a>
                </List>
                <Divider />
                <List>
                    <a className={classes.link} href="flast://apps/">
                        <ListItem button selected={'flast://apps/' === window.location.href}>
                            <ListItemIcon><AppsIcon /></ListItemIcon>
                            <ListItemText primary="アプリ" />
                        </ListItem>
                    </a>
                    <a className={classes.link}>
                        <ListItem button disabled>
                            <ListItemIcon><ShopIcon /></ListItemIcon>
                            <ListItemText primary="Flast Store" />
                        </ListItem>
                    </a>
                </List>
                <Divider />
                <List>
                    <ListItem button onClick={this.handleNestedListClick}>
                        <ListItemIcon><SettingsIcon /></ListItemIcon>
                        <ListItemText primary="設定" />
                        {this.state.open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </ListItem>
                    <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding dense>
                            <ListItem button selected={'flast://settings/' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings'; window.location.reload(); }}>
                                <ListItemText primary="ホーム" />
                            </ListItem>
                        </List>
                        <Divider />
                        <List component="div" disablePadding dense>
                            <ListItem button selected={'flast://settings/#design' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#design'; window.location.reload(); }}>
                                <ListItemText primary="デザイン" />
                            </ListItem>
                            <ListItem button selected={'flast://settings/#homePage' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#homePage'; window.location.reload(); }}>
                                <ListItemText primary="ホームページ" />
                            </ListItem>
                            <ListItem button selected={'flast://settings/#search' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#search'; window.location.reload(); }}>
                                <ListItemText primary="検索エンジン" />
                            </ListItem>
                            <ListItem button selected={'flast://settings/#pageSettings' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#pageSettings'; window.location.reload(); }}>
                                <ListItemText primary="ページ設定" />
                            </ListItem>
                            <ListItem button selected={'flast://settings/#adblock' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#adblock'; window.location.reload(); }}>
                                <ListItemText primary="広告ブロック" />
                            </ListItem>
                            <ListItem button selected={'flast://settings/#language' === window.location.href} className={classes.nested} onClick={() => { window.location.href = 'flast://settings/#language'; window.location.reload(); }}>
                                <ListItemText primary="言語" />
                            </ListItem>
                        </List>
                    </Collapse>
                    <a className={classes.link} href="flast://help/">
                        <ListItem button selected={'flast://help/' === window.location.href}>
                            <ListItemIcon><HelpIcon /></ListItemIcon>
                            <ListItemText primary="ヘルプ" />
                        </ListItem>
                    </a>
                </List>
                <Divider />
                <List>
                    <ListItem button selected={'flast://settings/#about' === window.location.href} onClick={() => { window.location.href = 'flast://settings/#about'; window.location.reload(); }}>
                        <ListItemIcon><InfoIcon /></ListItemIcon>
                        <ListItemText primary={`${window.getAppName()} について`} />
                    </ListItem>
                </List>
            </ThemeProvider>
        );

        return (
            <ThemeProvider theme={window.getDarkTheme() ? darkTheme : lightTheme}>
                <div className={classes.root}>
                    <CssBaseline />
                    <AppBar position="fixed" className={classes.appBar}>
                        <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="Open drawer"
                                edge="start"
                                onClick={this.handleDrawerToggle}
                                className={classes.menuButton}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="h6" noWrap className={classes.title}>{this.props.title}</Typography>
                            {this.props.buttons ? <div>{this.props.buttons}</div> : null}
                        </Toolbar>
                    </AppBar>
                    <nav className={classes.drawer}>
                        <Hidden smUp implementation="css">
                            <Drawer
                                container={this.props.container}
                                variant="temporary"
                                open={this.state.mobileOpen}
                                onClose={this.handleDrawerToggle}
                                classes={{ paper: classes.drawerPaper }}
                                ModalProps={{ keepMounted: true }}
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                        <Hidden xsDown implementation="css">
                            <Drawer
                                classes={{ paper: classes.drawerPaper }}
                                variant="permanent"
                                open
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                    </nav>
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <ThemeProvider theme={window.getDarkTheme() ? darkTheme : lightTheme}>
                            {this.props.children}
                        </ThemeProvider>
                    </main>
                </div>
            </ThemeProvider>
        );
    }
}

NavigationBar.propTypes = {
    classes: PropTypes.object.isRequired,
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(NavigationBar);