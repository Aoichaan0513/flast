const { app, shell, ipcMain, protocol, session, BrowserWindow, BrowserView, Menu, nativeImage, clipboard, dialog, Notification } = require('electron');
const path = require('path');
const { parse, format } = require('url');
const os = require('os');
const https = require('https');
const http = require('http');

const MainWindow = require('./Windows/MainWindow');

const pkg = require(`${app.getAppPath()}/package.json`);
const protocolStr = 'flast';
const fileProtocolStr = `${protocolStr}-file`;

const { download } = require('electron-dl');
const platform = require('electron-platform');
const localShortcut = require('electron-localshortcut');

const Config = require('electron-store');
const config = new Config({
    defaults: {
        design: {
            isHomeButton: false,
            isBookmarkBar: false,
            isDarkTheme: false,
            tabAccentColor: '#0a84ff',
            isCustomTitlebar: true,
            theme: 'default'
        },
        homePage: {
            isDefaultHomePage: true,
            defaultPage: `${protocolStr}://home`,
        },
        searchEngine: {
            defaultEngine: 'Google',
            searchEngines: [
                {
                    name: 'Google',
                    url: 'https://www.google.com/search?q=%s'
                },
                {
                    name: 'Bing',
                    url: 'https://www.bing.com/search?q=%s'
                },
                {
                    name: 'Yahoo! Japan',
                    url: 'https://search.yahoo.co.jp/search?p=%s'
                },
                {
                    name: 'goo',
                    url: 'https://search.goo.ne.jp/web.jsp?MT=%s'
                },
                {
                    name: 'OCN',
                    url: 'https://search.goo.ne.jp/web.jsp?MT=%s'
                },
                {
                    name: 'Baidu',
                    url: 'https://www.baidu.com/s?wd=%s'
                },
                {
                    name: 'Google Translate',
                    url: 'https://translate.google.com/?text=%s'
                },
                {
                    name: 'Youtube',
                    url: 'https://www.youtube.com/results?search_query=%s'
                },
                {
                    name: 'Twitter',
                    url: 'https://www.twitter.com/search?q=%s'
                },
                {
                    name: 'GitHub',
                    url: 'https://github.com/search?q=%s'
                },
                {
                    name: 'DuckDuckGo',
                    url: 'https://duckduckgo.com/?q=%s'
                },
                {
                    name: 'Yahoo',
                    url: 'https://search.yahoo.com/search?p=%s'
                },
                {
                    name: 'Amazon',
                    url: 'https://www.amazon.co.jp/s?k=%s'
                }
            ]
        },
        pageSettings: {
            defaultZoomSize: 1,
            media: -1,
            geolocation: -1,
            notifications: -1,
            midiSysex: -1,
            pointerLock: -1,
            fullscreen: 1,
            openExternal: -1,
            pages: {
                twitter: {
                    url: 'https://twitter.com/*',
                    oldDesign: false,
                    oldDesignIgnore: false
                }
            }
        },
        adBlock: {
            isAdBlock: true,
            disabledSites: []
        },
        language: 'ja',
        window: {
            isCloseConfirm: true,
            isMaximized: false,
            bounds: {
                width: 1100,
                height: 680
            }
        }
    },
});

const lang = require(`${app.getAppPath()}/langs/${config.get('language') != undefined ? config.get('language') : 'ja'}.js`);

const Datastore = require('nedb');
let db = {};
db.pageSettings = new Datastore({
    filename: path.join(app.getPath('userData'), 'Files', 'PageSettings.db'),
    autoload: true,
    timestampData: true
});

db.historys = new Datastore({
    filename: path.join(app.getPath('userData'), 'Files', 'History.db'),
    autoload: true,
    timestampData: true
});
db.downloads = new Datastore({
    filename: path.join(app.getPath('userData'), 'Files', 'Download.db'),
    autoload: true,
    timestampData: true
});
db.bookmarks = new Datastore({
    filename: path.join(app.getPath('userData'), 'Files', 'Bookmarks.db'),
    autoload: true,
    timestampData: true
});

db.apps = new Datastore({
    filename: path.join(app.getPath('userData'), 'Files', 'Apps.db'),
    autoload: true,
    timestampData: true
});

const { loadFilters, updateFilters, runAdblockService, removeAds } = require('./AdBlocker');

let floatingWindows = [];

getBaseWindow = (width = 1100, height = 680, minWidth = 500, minHeight = 360, x, y, frame = false) => {
    return new BrowserWindow({
        width, height, minWidth, minHeight, x, y, titleBarStyle: 'hidden', frame, fullscreenable: true,
        icon: `${__dirname}/static/app/icon.png`,
        show: false,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
            plugins: true,
            experimentalFeatures: true,
            contextIsolation: false,
        }
    });
}

loadSessionAndProtocol = () => {
    const ses = session.defaultSession;

    // setPermissionRequestHandler(ses, false);

    protocol.isProtocolHandled(protocolStr, (handled) => {
        if (!handled) {
            protocol.registerFileProtocol(protocolStr, (request, callback) => {
                const parsed = parse(request.url);

                return callback({
                    path: path.join(app.getAppPath(), 'pages', `${parsed.hostname}.html`),
                });
            }, (error) => {
                if (error) console.error('Failed to register protocol: ' + error);
            });
        }
    });

    protocol.isProtocolHandled(fileProtocolStr, (handled) => {
        if (!handled) {
            protocol.registerFileProtocol(fileProtocolStr, (request, callback) => {
                const parsed = parse(request.url);

                return callback({
                    path: path.join(app.getAppPath(), 'pages', 'static', parsed.pathname),
                });
            }, (error) => {
                if (error) console.error('Failed to register protocol: ' + error);
            });
        }
    });
}

loadSessionAndProtocolWithPrivateMode = (windowId) => {
    const ses = session.fromPartition(windowId);
    // ses.setUserAgent(ses.getUserAgent().replace(/ Electron\/[0-9\.]*/g, '') + ' PrivMode');

    // setPermissionRequestHandler(ses, true);

    ses.protocol.registerFileProtocol(protocolStr, (request, callback) => {
        const parsed = parse(request.url);

        return callback({
            path: path.join(app.getAppPath(), 'pages', `${parsed.hostname}.html`),
        });
    }, (error) => {
        if (error) console.error('Failed to register protocol: ' + error);
    });

    ses.protocol.registerFileProtocol(fileProtocolStr, (request, callback) => {
        const parsed = parse(request.url);

        return callback({
            path: path.join(app.getAppPath(), 'pages', 'static', parsed.pathname),
        });
    }, (error) => {
        if (error) console.error('Failed to register protocol: ' + error);
    });
}

/*
setPermissionRequestHandler = (ses, isPrivate = false) => {
    if (!isPrivate) {
        ses.setPermissionRequestHandler((webContents, permission, callback) => {
            const url = parse(webContents.getURL());

            db.pageSettings.findOne({ origin: `${url.protocol}//${url.hostname}` }, (err, doc) => {
                if (doc != undefined) {
                    if (permission == 'media' && doc.media != undefined && doc.media > -1)
                        return callback(doc.media === 0);
                    if (permission == 'geolocation' && doc.geolocation != undefined && doc.geolocation > -1)
                        return callback(doc.geolocation === 0);
                    if (permission == 'notifications' && doc.notifications != undefined && doc.notifications > -1)
                        return callback(doc.notifications === 0);
                    if (permission == 'midiSysex' && doc.midiSysex != undefined && doc.midiSysex > -1)
                        return callback(doc.midiSysex === 0);
                    if (permission == 'pointerLock' && doc.pointerLock != undefined && doc.pointerLock > -1)
                        return callback(doc.pointerLock === 0);
                    if (permission == 'fullscreen' && doc.fullscreen != undefined && doc.fullscreen > -1)
                        return callback(doc.fullscreen === 0);
                    if (permission == 'openExternal' && doc.openExternal != undefined && doc.openExternal > -1)
                        return callback(doc.openExternal === 0);
                } else {
                    if (config.get(`pageSettings.${permission}`) === null || config.get(`pageSettings.${permission}`) === -1) {
                        if (Notification.isSupported()) {
                            const notify = new Notification({
                                icon: path.join(app.getAppPath(), 'static', 'app', 'icon.png'),
                                title: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                                body: '詳細はここをクリックしてください。',
                                silent: true
                            });

                            notify.show();

                            notify.on('click', (e) => {
                                dialog.showMessageBox({
                                    type: 'info',
                                    title: '権限の要求',
                                    message: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                                    detail: `要求内容: ${permission}`,
                                    checkboxLabel: 'このサイトでは今後も同じ処理をする',
                                    checkboxChecked: false,
                                    noLink: true,
                                    buttons: ['はい / Yes', 'いいえ / No'],
                                    defaultId: 0,
                                    cancelId: 1
                                }, (res, checked) => {
                                    if (checked) {
                                        if (permission == 'media')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, media: res }, { upsert: true });
                                        if (permission == 'geolocation')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, geolocation: res }, { upsert: true });
                                        if (permission == 'notifications')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, notifications: res }, { upsert: true });
                                        if (permission == 'midiSysex')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, midiSysex: res }, { upsert: true });
                                        if (permission == 'pointerLock')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, pointerLock: res }, { upsert: true });
                                        if (permission == 'fullscreen')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, fullscreen: res }, { upsert: true });
                                        if (permission == 'openExternal')
                                            db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, openExternal: res }, { upsert: true });
                                    }
                                    return callback(res === 0);
                                });
                            });
                            notify.on('close', (e) => {
                                return callback(false);
                            });
                        } else {
                            dialog.showMessageBox({
                                type: 'info',
                                title: '権限の要求',
                                message: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                                detail: `要求内容: ${permission}`,
                                checkboxLabel: 'このサイトでは今後も同じ処理をする',
                                checkboxChecked: false,
                                noLink: true,
                                buttons: ['はい / Yes', 'いいえ / No'],
                                defaultId: 0,
                                cancelId: 1
                            }, (res, checked) => {
                                if (checked) {
                                    if (permission == 'media')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, media: res }, { upsert: true });
                                    if (permission == 'geolocation')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, geolocation: res }, { upsert: true });
                                    if (permission == 'notifications')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, notifications: res }, { upsert: true });
                                    if (permission == 'midiSysex')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, midiSysex: res }, { upsert: true });
                                    if (permission == 'pointerLock')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, pointerLock: res }, { upsert: true });
                                    if (permission == 'fullscreen')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, fullscreen: res }, { upsert: true });
                                    if (permission == 'openExternal')
                                        db.pageSettings.update({ origin: `${url.protocol}//${url.hostname}` }, { origin: `${url.protocol}//${url.hostname}`, openExternal: res }, { upsert: true });
                                }
                                return callback(res === 0);
                            });
                        }
                    } else if (config.get(`pageSettings.${permission}`) === 0) {
                        return callback(false);
                    } else if (config.get(`pageSettings.${permission}`) === 1) {
                        return callback(true);
                    }
                }
            });
        });
    } else {
        ses.setPermissionRequestHandler((webContents, permission, callback) => {
            const url = parse(webContents.getURL());

            if (config.get(`pageSettings.${permission}`) === null || config.get(`pageSettings.${permission}`) === -1) {
                if (Notification.isSupported()) {
                    const notify = new Notification({
                        icon: path.join(app.getAppPath(), 'static', 'app', 'icon.png'),
                        title: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                        body: '詳細はここをクリックしてください。\nプライベート ウィンドウ',
                        silent: true
                    });

                    notify.show();

                    notify.on('click', (e) => {
                        dialog.showMessageBox({
                            type: 'info',
                            title: '権限の要求',
                            message: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                            detail: `要求内容: ${permission}`,
                            noLink: true,
                            buttons: ['はい / Yes', 'いいえ / No'],
                            defaultId: 0,
                            cancelId: 1
                        }, (res) => {
                            return callback(res === 0);
                        });
                    });
                    notify.on('close', (e) => {
                        return callback(false);
                    });
                } else {
                    dialog.showMessageBox({
                        type: 'info',
                        title: '権限の要求',
                        message: `${url.protocol}//${url.hostname} が権限を要求しています。`,
                        detail: `要求内容: ${permission}`,
                        noLink: true,
                        buttons: ['はい / Yes', 'いいえ / No'],
                        defaultId: 0,
                        cancelId: 1
                    }, (res) => {
                        return callback(res === 0);
                    });
                }
            } else if (config.get(`pageSettings.${permission}`) === 0) {
                return callback(false);
            } else if (config.get(`pageSettings.${permission}`) === 1) {
                return callback(true);
            }
        });
    }
}
*/

module.exports = class WindowManager {
    constructor() {
        this.windows = new Map();

        this.currentWindow = null;

        ipcMain.on('window-add', (e, args) => {
            this.addWindow(args.isPrivate, args.url);
        });

        ipcMain.on('appWindow-add', (e, args) => {
            this.addAppWindow(args.url);
        });

        ipcMain.on('window-fullScreen', (e, args) => {
            this.addWindow(args.isPrivate);
        });

        ipcMain.on('window-fixBounds', (e, args) => {
            this.windows.forEach((value, key) => {
                value.window.fixBounds();
            });
        });

        ipcMain.on('window-change-settings', (e, args) => {
            this.windows.forEach((value, key) => {
                value.window.webContents.send('window-change-settings', {});
                value.window.fixBounds();
            })
        });

        ipcMain.on('update-filters', (e, args) => {
            updateFilters();
        });

        ipcMain.on('data-history-get', (e, args) => {
            db.historys.find({}).sort({ createdAt: -1 }).exec((err, docs) => {
                e.sender.send('data-history-get', { historys: docs });
            });
        });

        ipcMain.on('data-history-clear', (e, args) => {
            db.historys.remove({}, { multi: true });
        });

        ipcMain.on('data-downloads-get', (e, args) => {
            db.downloads.find({}).sort({ createdAt: -1 }).exec((err, docs) => {
                e.sender.send('data-downloads-get', { downloads: docs });
            });
        });

        ipcMain.on('data-downloads-clear', (e, args) => {
            db.downloads.remove({}, { multi: true });
        });

        ipcMain.on('data-bookmarks-get', (e, args) => {
            db.bookmarks.find({ isPrivate: args.isPrivate }).sort({ createdAt: -1 }).exec((err, docs) => {
                e.sender.send('data-bookmarks-get', { bookmarks: docs });
            });
        });

        ipcMain.on('data-bookmarks-clear', (e, args) => {
            db.bookmarks.remove({}, { multi: true });
        });

        ipcMain.on('data-apps-add', (e, args) => {
            db.apps.update({ id: args.id }, { id: args.id, name: args.name, description: args.description, url: args.url }, { upsert: true });

            db.apps.find({}).sort({ createdAt: -1 }).exec();
        });

        ipcMain.on('data-apps-remove', (e, args) => {
            db.apps.remove({ id: args.id }, {});
        });

        ipcMain.on('data-apps-get', (e, args) => {
            db.apps.find({}).sort({ createdAt: -1 }).exec((err, docs) => {
                e.sender.send('data-apps-get', { apps: docs });
            });
        });

        ipcMain.on('data-apps-is', (e, args) => {
            db.apps.find({ id: args.id }).exec((err, docs) => {
                e.sender.send('data-apps-is', { id: args.id, isInstalled: (docs.length > 0 ? true : false) });
            });
        });

        ipcMain.on('data-apps-clear', (e, args) => {
            db.apps.remove({}, { multi: true });
        });

        ipcMain.on('clear-browsing-data', () => {
            const ses = session.defaultSession;
            ses.clearCache();

            ses.clearStorageData({
                storages: [
                    'appcache',
                    'cookies',
                    'filesystem',
                    'indexdb',
                    'localstorage',
                    'shadercache',
                    'websql',
                    'serviceworkers',
                    'cachestorage',
                ],
            });

            config.clear();
            db.pageSettings.remove({}, { multi: true });

            db.historys.remove({}, { multi: true });
            db.downloads.remove({}, { multi: true });
            db.bookmarks.remove({}, { multi: true });
            db.apps.remove({}, { multi: true });
        });
    }

    getWindows = () => {
        return this.windows;
    }

    getCurrentWindow = () => {
        return this.currentWindow;
    }

    addWindow = (isPrivate = false, url = (config.get('homePage.isDefaultHomePage') ? `${protocolStr}://home/` : config.get('homePage.defaultPage'))) => {
        loadSessionAndProtocol();
        loadFilters();

        const window = new MainWindow(this, isPrivate, db, url);
        const id = window.id;

        this.windows.set(id, { window, isPrivate });
        window.on('closed', () => { this.windows.delete(id); });

        return window;
    }

    addAppWindow = (url = config.get('homePage.defaultPage')) => {
        loadSessionAndProtocol();
        loadFilters();

        const { width, height, x, y } = config.get('window.bounds');
        const window = getBaseWindow(config.get('window.isMaximized') ? 1110 : width, config.get('window.isMaximized') ? 680 : height, 500, 360, x, y, !config.get('design.isCustomTitlebar'));
        const id = window.id;

        config.get('window.isMaximized') && window.maximize();

        const startUrl = process.env.ELECTRON_START_URL || format({
            pathname: path.join(__dirname, '/../build/index.html'), // 警告：このファイルを移動する場合ここの相対パスの指定に注意してください
            protocol: 'file:',
            slashes: true,
            hash: `/app/${id}/${encodeURIComponent(url)}`,
        });

        window.loadURL(startUrl);

        window.once('ready-to-show', () => { window.show(); });

        window.on('focus', () => { window.webContents.send(`window-focus-${id}`, {}); });
        window.on('blur', () => { window.webContents.send(`window-blur-${id}`, {}); });
    }

    fixBounds = (window) => {
        if (window.getBrowserView() == undefined) return;
        const view = window.getBrowserView();

        const { width, height } = window.getContentBounds();

        view.setAutoResize({ width: true, height: true });
        if (window.getFloatingWindow()) {
            window.setMinimizable(false);
            window.setMaximizable(false);
            window.setAlwaysOnTop(true);
            window.setVisibleOnAllWorkspaces(true);
            view.setBounds({
                x: 1,
                y: 1,
                width: width - 2,
                height: height - 2,
            });
        } else {
            window.setMinimizable(true);
            window.setMaximizable(true);
            window.setAlwaysOnTop(false);
            window.setVisibleOnAllWorkspaces(false);

            if (window.isFullScreen()) {
                view.setBounds({
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                });
            } else {
                view.setBounds({
                    x: window.isMaximized() ? 0 : config.get('design.isCustomTitlebar') ? 1 : 0,
                    y: window.isMaximized() ? this.getHeight(true, height) : config.get('design.isCustomTitlebar') ? this.getHeight(true, height) + 1 : this.getHeight(true, height),
                    width: window.isMaximized() ? width : config.get('design.isCustomTitlebar') ? width - 2 : width,
                    height: window.isMaximized() ? this.getHeight(false, height) : (config.get('design.isCustomTitlebar') ? (this.getHeight(false, height)) - 2 : (this.getHeight(false, height)) - 1),
                });
            }
        }
        view.setAutoResize({ width: true, height: true });
    }

    getHeight = (b, height) => {
        const titleBarHeight = 33;
        const toolBarHeight = 40;

        const baseBarHeight = titleBarHeight + toolBarHeight;
        const bookMarkBarHeight = 28;

        return b ? (config.get('design.isBookmarkBar') ? (baseBarHeight + bookMarkBarHeight) : baseBarHeight) : (height - (config.get('design.isBookmarkBar') ? (baseBarHeight + bookMarkBarHeight) : baseBarHeight));
    }
}