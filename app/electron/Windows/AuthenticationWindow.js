const { app, shell, ipcMain, protocol, session, BrowserWindow, BrowserView, Menu, nativeImage, clipboard, dialog, Notification } = require('electron');
const path = require('path');
const { parse, format } = require('url');
const os = require('os');
const https = require('https');
const http = require('http');

const pkg = require(`${app.getAppPath()}/package.json`);
const protocolStr = 'flast';
const fileProtocolStr = `${protocolStr}-file`;

const { download } = require('electron-dl');
const platform = require('electron-platform');
const localShortcut = require('electron-localshortcut');

const Config = require('electron-store');
const config = new Config();

module.exports = class AuthenticationWindow extends BrowserWindow {
    constructor(appWindow, windowId) {
        super({
            width: 320,
            height: 220,
            frame: false,
            resizable: false,
            transparent: true,
            show: false,
            fullscreenable: false,
            webPreferences: {
                nodeIntegration: true,
                contextIsolation: false,
            },
            skipTaskbar: true,
        });

        this.appWindow = appWindow;
        this.windowId = windowId;

        const startUrl = process.env.ELECTRON_START_URL || format({
            pathname: path.join(__dirname, '/../../build/index.html'),
            protocol: 'file:',
            slashes: true,
            hash: `/authentication/${windowId}`,
        });

        this.loadURL(startUrl);
        this.setParentWindow(appWindow);

        // this.show();
        this.fixBounds();
    }

    isClosed = () => {
        return !this.isDestroyed();
    }

    showWindow = (loginCallback) => {
        if (this.isDestroyed()) return;
        this.hide();
        this.show();
        this.fixBounds();

        ipcMain.once(`authWindow-close-${this.windowId}`, (e, result) => {
            this.hide();
            this.appWindow.focus();
        });

        ipcMain.once(`authWindow-result-${this.windowId}`, (e, arg) => {
            loginCallback(arg.user, arg.pass);
            if (!this.isDestroyed())
                this.hide();
        });

    }

    fixBounds = () => {
        if (!this.isVisible()) return;
        const bounds = this.appWindow.getContentBounds();
        this.setBounds({ x: bounds.x + 148, y: bounds.y + 70 + 1 });
    }
}