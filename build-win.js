const builder = require('electron-builder');
const fs = require('fs');
const pkg = JSON.parse(fs.readFileSync('./app/package.json', 'utf8'));

builder.build({
    config: {
        'appId': pkg.flast_package_id,
        'productName': pkg.name,
        'copyright': `Copyright 2019 ${pkg.author.name}. All rights reserved.`,
        'asar': true,
        'directories': {
            'output': 'dist',
            'buildResources': 'static'
        },
        'publish': {
            'provider': 'generic',
            'url': `http://aoichaan0513.xyz/flast/${process.platform}/${process.arch}/${pkg.flast_channel}`,
            'channel': pkg.flast_channel
        },
        'fileAssociations': [
            {
                'name': 'Document',
                'description': pkg.name,
                'role': 'Viewer',
                'ext': ['html', 'htm', 'php']
            }
        ],
        'nsis': {
            'include': 'static/installer.nsh',
            'installerIcon': 'static/icon.ico',
            'uninstallerIcon': 'static/icon.ico'
        },
        'win': {
            'target': 'nsis',
            'icon': 'static/icon.ico',
        },
    },
});